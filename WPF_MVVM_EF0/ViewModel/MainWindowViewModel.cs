﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.ComponentModel;
using WPF_MVVM_EF0.Model;
using System.Runtime.CompilerServices;

namespace WPF_MVVM_EF0.ViewModel
{
    class MainWindowViewModel : INotifyPropertyChanged
    {
        AuthorBookEntities ctx = new AuthorBookEntities();

        private List<Author> _authors;
        
        public List<Author> Authors
        {
            get
            {
                return _authors;
            }
            set
            {
                _authors = value;
                NotifyPropertyChanged();
            }
        }

        private Author _selectedAuthor;
        public Author SelectedAuthor
        {
            get
            {
                return _selectedAuthor;
            }
            set
            {
                _selectedAuthor = value;
                NotifyPropertyChanged();
                FillBook();
            }
        }

        private List<Book> _books;
        public List<Book> Books
        {
            get
            {
                return _books;
            }
            set
            {
                _books = value;
                NotifyPropertyChanged();
            }
        }

        private Book _selectedBook;
        public Book SelectedBook
        {
            get
            {
                return _selectedBook;
            }
            set
            {
                _selectedBook = value;
                NotifyPropertyChanged();
            }
        }

        private void FillBook()
        {
            Author author = this.SelectedAuthor;
            var b = (from book in ctx.Books
                     orderby book.Title
                     where book.AuthorId == author.AuthorId
                     select book).ToList();
            this.Books = b;
        }


        public MainWindowViewModel()
        {
            FillAuthors();
        }

        private void FillAuthors()
        {
            var a = (from author in ctx.Authors
                         select author).ToList();
            this.Authors = a;
        }


        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD1
{
    public class StudentRepository
    {
        private DemoWPFEntities studentContext = null;

        public StudentRepository()
        {
            studentContext = new DemoWPFEntities();
        }

        public Student Get(int id)
        {
            return studentContext.Students.Find(id);
        }

        public List<Student> GetAll()
        {
            return studentContext.Students.ToList();
        }

        public void AddStudent(Student student)
        {
            if (student != null)
            {
                studentContext.Students.Add(student);
                studentContext.SaveChanges();
            }
        }

        public void UpdateStudent(Student student)
        {
            var studentFind = this.Get(student.ID);
            if (studentFind != null)
            {
                studentFind.Name = student.Name;
                studentFind.Contact = student.Contact;
                studentFind.Age = student.Age;
                studentFind.Address = student.Address;
                studentContext.SaveChanges();
            }
        }

        public void RemoveStudent(int id)
        {
            var studentObj = studentContext.Students.Find(id);
            if (studentObj != null)
            {
                studentContext.Students.Remove(studentObj);
                studentContext.SaveChanges();
            }
        }

    }
}










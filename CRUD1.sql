create database DemoWPF
go
use DemoWPF
go
/****** Object:  Table [dbo].[Students]    Script Date: 3/31/2018 11:24:23 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[Students](
 [ID] [int] IDENTITY(1,1) NOT NULL,
 [Name] [varchar](50) NULL,
 [Age] [int] NULL,
 [Address] [varchar](50) NULL,
 [Contact] [varchar](50) NULL,
 CONSTRAINT [PK_Students] PRIMARY KEY CLUSTERED 
(
 [ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO
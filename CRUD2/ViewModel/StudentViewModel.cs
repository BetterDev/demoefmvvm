﻿using CRUD2.DataAccess;
using CRUD2.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace CRUD2.ViewModel
{
    public class StudentViewModel
    {
        private ICommand _saveCommand;
        private ICommand _resetCommand;
        private ICommand _editCommand;
        private ICommand _deleteCommand;
        private StudentRepository _repository;
        private Student _studentEntity = null;
        public StudentRecord studentRecord { get; set; }
        public StudentEntities studentEntities { get; set; }


        public StudentViewModel()
        {
            _studentEntity = new Student();
            _repository = new StudentRepository();
            studentRecord = new StudentRecord();
            GetAll();
        }

        public void GetAll()
        {
            studentRecord.StudentRecords = new ObservableCollection<StudentRecord>();
            _repository.GetAll().ForEach(data => studentRecord.StudentRecords.Add(new StudentRecord()
            {
                Id = data.ID,
                Name = data.Name,
                Address = data.Address,
                Age = Convert.ToInt32(data.Age),
                Contact = data.Contact
            }));
        }

        public void ResetData()
        {
            studentRecord.Name = string.Empty;
            studentRecord.Id = 0;
            studentRecord.Address = string.Empty;
            studentRecord.Contact = string.Empty;
            studentRecord.Age = 0;
        }

        public void DeleteStudent(int Id)
        {
            if (MessageBox.Show("Confirm delete of this record?","Student",MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                try
                {
                    _repository.RemoveStudent(Id);
                    MessageBox.Show("Successfully delete record.");
                }
                catch (Exception e)
                {
                    MessageBox.Show("Error occured while deleting.");
                }
                finally
                {
                    GetAll();
                }
            }
        }

        public void SaveData()
        {
            if (studentRecord != null)
            {
                _studentEntity.Name = studentRecord.Name;
                _studentEntity.Age = studentRecord.Age;
                _studentEntity.Address = studentRecord.Address;
                _studentEntity.Contact = studentRecord.Contact;

                try
                {
                    if (studentRecord.Id <= 0)
                    {
                        _repository.AddStudent(_studentEntity);
                        MessageBox.Show("Successfully save new record");
                    }
                    else
                    {
                        _studentEntity.ID = studentRecord.Id;
                        _repository.UpdateStudent(_studentEntity);
                        MessageBox.Show("Successfully update record");
                    }
                }
                catch (Exception e)
                {
                    MessageBox.Show("Error occured while saving. " + e.InnerException);
                }
                finally
                {
                    GetAll();
                    ResetData();
                }
            }
        }

        public void EditData(int Id)
        {
            var studentFind = _repository.Get(Id);
            studentRecord.Id = studentFind.ID;
            studentRecord.Name = studentFind.Name;
            studentRecord.Age = (int)studentFind.Age;
            studentRecord.Address = studentFind.Address;
            studentRecord.Contact = studentFind.Contact;
        }


        public ICommand ResetCommand
        {
            get
            {
                if(_resetCommand == null)
                {
                    _resetCommand = new RelayCommand(param => ResetData(), null);
                }
                return _resetCommand;
            }
        }

        public ICommand SaveCommand
        {
            get
            {
                if (_saveCommand == null)
                    _saveCommand = new RelayCommand(param => SaveData(), null);
                return _saveCommand;
            }
        }

        public ICommand EditCommand
        {
            get
            {
                if (_editCommand == null)
                    _editCommand = new RelayCommand(param => EditData((int)param), null);
                return _editCommand;
            }
        }

        public ICommand DeleteCommand
        {
            get
            {
                if (_deleteCommand == null)
                    _deleteCommand = new RelayCommand(param => DeleteStudent((int)param), null);
                return _deleteCommand;
            }
        }


    }
}

﻿using CRUD2.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD2.DataAccess
{
    public class StudentRepository
    {
        private StudentEntities studentContext = null;

        public StudentRepository()
        {
            studentContext = new StudentEntities();
        }

        public Student Get(int Id)
        {
            return studentContext.Students.Find(Id);
        }

        public List<Student> GetAll()
        {
            return studentContext.Students.ToList();
        }

        public void AddStudent(Student student)
        {
            if (student != null)
            {
                studentContext.Students.Add(student);
                studentContext.SaveChanges();
            }
        }

        public void UpdateStudent(Student student)
        {
            var studentFind = this.Get(student.ID);
            if (studentFind != null)
            {
                studentFind.Name = student.Name;
                studentFind.Contact = student.Contact;
                studentFind.Age = student.Age;
                studentFind.Address = student.Address;
                studentContext.SaveChanges();
            }
        }

        public void RemoveStudent(int Id)
        {
            var studentFind = studentContext.Students.Find(Id);
            if (studentFind != null)
            {
                studentContext.Students.Remove(studentFind);
                studentContext.SaveChanges();
            }
        }

    }
}
